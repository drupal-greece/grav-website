---
title: 'Έργα μας'
---

## Υπηρεσίες που παρέχονται από την κοινότητα

Οι παγκοσμίου βελινεκούς ιστοσελίδες [dgo.re](https://dgo.re) και [dgo.to](https://dgo.to) αναπτύσονται και συντηρούνται από τα μέλη της Ελληνικής Κοινότητας Drupal.

* [https://www.drupal.org/project/dgore](https://www.drupal.org/project/dgore)
* [https://www.drupal.org/project/dgoto](https://www.drupal.org/project/dgoto)

## Drupal Modules

Τα μέλη της Ελληνικής Κοινότητας είναι επίσης πολύ ενεργά στην ανάπτυξη του Drupal Core καθώς και των modules που το επεκτείνουν. Παρακάτω βρίσκεται μια μικρή λίστα:

* [https://www.drupal.org/project/admin_feedback](https://www.drupal.org/project/admin_feedback)
* [https://www.drupal.org/project/create_user_permission](https://www.drupal.org/project/create_user_permission)
* [https://www.drupal.org/project/human_decimal](https://www.drupal.org/project/human_decimal)
* [https://www.drupal.org/project/migrate_sourceid](https://www.drupal.org/project/migrate_sourceid)
* [https://www.drupal.org/project/pace](https://www.drupal.org/project/pace)
* [https://www.drupal.org/project/paragraphs_asymmetric_translation_widgets](https://www.drupal.org/project/paragraphs_asymmetric_translation_widgets)
* [https://www.drupal.org/project/rua](https://www.drupal.org/project/rua)
* [https://www.drupal.org/project/scrollbar](https://www.drupal.org/project/scrollbar)

### Commerce & Modules για επικοινωνία με ελληνικές τράπεζες
* [https://www.drupal.org/project/commerce_alphabank_redirect](https://www.drupal.org/project/commerce_alphabank_redirect)
* [https://www.drupal.org/project/commerce_eurobank_redirect](https://www.drupal.org/project/commerce_eurobank_redirect)
* [https://www.drupal.org/project/commerce_winbank_redirect](https://www.drupal.org/project/commerce_winbank_redirect)
* [https://www.drupal.org/project/commerce_static_checkout_url](https://www.drupal.org/project/commerce_static_checkout_url)


## Οδηγοί και συλλογές

* [drupaltools.com: A list of popular open source and free tools that can help people accomplish Drupal related tasks.](https://drupaltools.com)
* [theodorosploumis/awesome-drupal](https://github.com/theodorosploumis/awesome-drupal)
* [theodorosploumis/drupal-best-practices](https://github.com/theodorosploumis/drupal-best-practices)
* [eworx-org/drupal-js - Best practices to integrate a Drupal 8.x+ backend with a JS frontend](https://github.com/eworx-org/drupal-js)
