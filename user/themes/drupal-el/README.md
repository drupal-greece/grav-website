# Drupal El Theme

The **Drupal El** Theme is for the [Grav CMS](http://github.com/getgrav/grav)-based 
website [Drupal Greece](https://drupal.org.gr). It is an inherited theme (subtheme) 
of the core Quark theme.

## Adding CSS
Just create a css/custom.css file, and it should be auto-discovered and included.
